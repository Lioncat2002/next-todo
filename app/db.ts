import { PrismaClient } from "@prisma/client";

const prismaGlobal = global as unknown as {
    prisma: PrismaClient | undefined
}

export const prisma = prismaGlobal.prisma??
                        new PrismaClient({
                            log: ["query"]
                        }) 
prismaGlobal.prisma=prisma